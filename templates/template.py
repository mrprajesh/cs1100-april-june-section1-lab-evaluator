#!!!!!!!!!!!! @This file is not for student use. Do not modify it, 0 Marks will be awarded if found guilty.@!!!!!!!!!!!!!!

def decrypt(message,passkey):
    '''
    Decrypts the encrypted message using the passkey.
    '''
    from cryptography.fernet import Fernet
    decoder = Fernet(passkey)
    return decoder.decrypt(message).decode()

import getpass
passkey = getpass.getpass(prompt="Paste your passkey: ")

tc_enc = {0}
code = {1}

try:
    exec(decrypt(code,passkey))
except:
    print("Problem running the code, The code has been altered or wrong key was entered")
