
from cryptography.fernet import Fernet

from os.path import dirname, join, abspath
import sys

HOME = abspath(join(dirname(__file__),'..'))
sys.path.append(HOME)

from bin.utility import PROJECT_PATH

passkey = Fernet.generate_key()
print(f"Here is your passkey: {passkey}")

with open(f'{PROJECT_PATH}/pass.key','wb') as f:
    f.write(passkey)
